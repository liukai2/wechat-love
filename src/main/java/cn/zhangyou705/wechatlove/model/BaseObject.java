package cn.zhangyou705.wechatlove.model;

public class BaseObject {

    public BaseObject() {
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getSimpleName());
        builder.append(" [");
        this.toStringAppendFields(builder);
        builder.append("]");
        return builder.toString();
    }

    protected void toStringAppendFields(StringBuilder builder) {
    }
}
