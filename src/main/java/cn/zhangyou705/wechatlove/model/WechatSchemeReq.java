package cn.zhangyou705.wechatlove.model;

import lombok.Data;

import java.util.Map;

/**
 * @Author: xyshmily
 * @Date: 2022/5/16 11:24
 */
@Data
public class WechatSchemeReq extends BaseObject {
    private Map<String, String> jump_wxa;
}
