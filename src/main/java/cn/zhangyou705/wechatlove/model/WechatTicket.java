package cn.zhangyou705.wechatlove.model;

import lombok.Data;

/**
 * @Author: xyshmily
 * @Date: 2022/5/10 10:05
 */
@Data
public class WechatTicket extends AbstractObject {

    private String ticket;

    private String errmsg;

    private Integer errcode;

    private Integer expires_in;
}
