package cn.zhangyou705.wechatlove.model;

import lombok.Data;

/**
 * Created by guocc on 2022/6/21.
 */
@Data
public class WechatSchemeModel extends AbstractObject {
    private String tenantId;
    private WechatSchemeReq wechatSchemeReq;
}
