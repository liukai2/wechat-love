package cn.zhangyou705.wechatlove.model;

import lombok.Data;

/**
 * @Author: xyshmily
 * @Date: 2022/5/10 10:00
 */
@Data
public class WechatToken extends AbstractObject {

    private String access_token;

    private String errmsg;

    private Integer errcode;

    private Integer expires_in;
}
