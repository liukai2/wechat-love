package cn.zhangyou705.wechatlove.model;

import lombok.Data;

/**
 * @Author: xyshmily
 * @Date: 2022/5/16 10:51
 */
@Data
public class WechatScheme extends AbstractObject {

    private String openlink;

    private String errmsg;

    private Integer errcode;

}
