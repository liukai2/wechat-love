package cn.zhangyou705.wechatlove.model;

import lombok.Data;

import java.util.UUID;

/**
 * @Author: xyshmily
 * @Date: 2022/5/9 18:26
 */
@Data
public class WechatSignVO extends BaseObject {

    private String url;

    private String nonceStr;

    private String timestamp;

    private String signature;

    private String appId;

    public WechatSignVO(String url) {
        this.url = url;
        this.nonceStr = UUID.randomUUID().toString();
        this.timestamp = Long.toString(System.currentTimeMillis() / 1000);
    }
}
