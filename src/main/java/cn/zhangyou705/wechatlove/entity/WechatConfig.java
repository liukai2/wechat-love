package cn.zhangyou705.wechatlove.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

//import javax.persistence.Entity;

/**
 * @Author: xyshmily
 * @Date: 2022/5/9 18:21
 */
@Data
//@Entity
@Component
@ConfigurationProperties(prefix = "wechat")
public class WechatConfig  {

    /**
     * 微信企业id （企业微信）
     */
    @Value("${wechat.appId}")
    private String corpId;

//    /**
//     * 企业密钥 （企业微信）
//     */
//    private String corpSecret;
//
//    /**
//     * 企业Id
//     */
//    private String tenantId;
//
//    /**
//     * 微信id
//     */
//    private String appId;
//
//    /**
//     * 微信密钥
//     */
//    private String appSecret;
//
//    /**
//     * 商户描述
//     */
//    private String description;
//    /**
//     * 微信服务号 appId
//     */
//    private String wxServiceAppId;
//    /**
//     * 微信服务号  secrect
//     */
//    private String wxServiceAppSecrect;
//
//    private String weChatOriginalId;
//
//    private String weChatServiceOriginalId;
}