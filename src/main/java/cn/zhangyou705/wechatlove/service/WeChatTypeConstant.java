package cn.zhangyou705.wechatlove.service;

/**
 * 微信类别常量
 */
public interface WeChatTypeConstant {
    String SERVICE="service";
    String PERSIONAL_WECHAT="wechat";
    String CORP_WECHAT="corp";
}
