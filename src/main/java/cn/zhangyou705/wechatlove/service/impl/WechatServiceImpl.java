//package cn.zhangyou705.wechatlove.service.impl;
//
//import cn.zhangyou705.wechatlove.entity.WechatConfig;
//import cn.zhangyou705.wechatlove.model.*;
//import cn.zhangyou705.wechatlove.service.WeChatTypeConstant;
//import cn.zhangyou705.wechatlove.service.WechatService;
//import com.google.common.cache.Cache;
//import com.google.common.cache.CacheBuilder;
////import com.newbanker.blackcat.wechat.repository.WechatConfigRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Service;
//import org.springframework.util.Assert;
//import org.springframework.util.StringUtils;
//import org.springframework.web.client.RestTemplate;
//
//import javax.annotation.PostConstruct;
//import java.io.UnsupportedEncodingException;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.Formatter;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.TimeUnit;
//
///**
// * @Author: xyshmily
// * @Date: 2022/5/9 18:31
// */
//@Slf4j
//@Service
////@ConditionalOnProperty(prefix = "wechat",name ="env",havingValue = "nb",matchIfMissing =true)
//public class WechatServiceImpl implements WechatService {
//
//    @Autowired
//    RestTemplate restTemplate;
//
//    @Value("${wechat.mp.tokenUrl:}")
//    String tokenUrl;
//
//    @Value("${wechat.mp.jsapiTicketUrl:}")
//    String jsapiTicketUrl;
//
//    @Value("${wechat.mp.wxJsapiTicketUrl:}")
//    String wxJsapiTicketUrl;
//
//    @Value("${wechat.mp.wxTokenUrl:}")
//    String wxTokenUrl;
//
//    @Value("${wechat.mp.schemeUrl:}")
//    String schemeUrl;
//
////    @Autowired
////    WechatConfigRepository wechatConfigRepository;
//
//    Cache<String, String> tokenCacheMap;
//
//    Cache<String, String> jsTicketCacheMap;
//
//    static final int DELAY_SECONDS = 7200;
//
//    static final String TENANT_WECHAT_KEY = "tenant_wechatconfig";
//
//    static final String SIGN_FORMAT = "jsapi_ticket=%s&noncestr=%s&timestamp=%s&url=%s";
//
//    @Autowired
//    RedisTemplate redisTemplate;
//
//    @Value("${server.appName:aap}")
//    String serverName;
//
//    @Value("${env:}")
//    String env;
//
//    String prefixKey;
//
//    /**
//     * 获取redis key
//     *
//     * @param format
//     * @param value
//     * @return
//     */
//    String getKey(String format, String... value) {
//        if (StringUtils.isEmpty(prefixKey)) {
//            prefixKey = serverName.concat("_").concat(env).concat("_");
//        }
//        return prefixKey.concat(String.format(format, value));
//    }
//
//    @PostConstruct
//    public void init() {
////        refreshWechatConfigMap();
//        tokenCacheMap = CacheBuilder.newBuilder().expireAfterWrite(DELAY_SECONDS, TimeUnit.SECONDS).build();
//        jsTicketCacheMap = CacheBuilder.newBuilder().expireAfterWrite(DELAY_SECONDS, TimeUnit.SECONDS).build();
//    }
//
//    /**
//     * 刷新配置缓存信息
//     */
////    @Override
////    public void refreshWechatConfigMap() {
////        Map<String, WechatConfig> wechatConfigMap = new ConcurrentHashMap<>();
////        List<WechatConfig> wechatConfigs = wechatConfigRepository.findAll();
////        wechatConfigs.forEach(wechatConfig -> {
////            wechatConfigMap.put(wechatConfig.getTenantId(), wechatConfig);
////        });
////        redisTemplate.opsForHash().putAll(getKey(TENANT_WECHAT_KEY), wechatConfigMap);
////    }
//
//    WechatConfig getWechatConfig(String tenantId) {
//        return new WechatConfig();
//    }
//
//    @Override
//    public String getAccessToken(WechatConfig wechatConfig) {
//        WechatToken wechatToken = restTemplate.getForObject(tokenUrl, WechatToken.class, wechatConfig.getCorpId(), wechatConfig.getCorpSecret());
//        log.debug("getAccessToken.[wechatToken={}]", wechatToken);
//        String accessToken = wechatToken.getAccess_token();
//        if (StringUtils.isEmpty(accessToken)) {
//            throw new IllegalArgumentException(wechatToken.getErrmsg());
//        }
//        tokenCacheMap.put(wechatConfig.getCorpId(), accessToken);
//        return accessToken;
//    }
//
//    @Override
//    public String getJsapiTicket(WechatConfig wechatConfig) {
//        String corpId = wechatConfig.getCorpId();
//        String jsTicket = jsTicketCacheMap.getIfPresent(corpId);
//        if (StringUtils.isEmpty(jsTicket)) {
//            String accessToken = tokenCacheMap.getIfPresent(corpId);
//            if (StringUtils.isEmpty(accessToken)) {
//                accessToken = getAccessToken(wechatConfig);
//            }
//            WechatTicket wechatTicket = restTemplate.getForObject(jsapiTicketUrl, WechatTicket.class, accessToken);
//            log.debug("getJsapiTicket.[wechatTicket={}]", wechatTicket);
//            jsTicket = wechatTicket.getTicket();
//            if (StringUtils.isEmpty(jsTicket)) {
//                // 处理极端场景 access_token 刚好失效
//                String accessTokenRefresh = getAccessToken(wechatConfig);
//                wechatTicket = restTemplate.getForObject(jsapiTicketUrl, WechatTicket.class, accessTokenRefresh);
//                log.debug("getJsapiTicket 2 .[wechatTicket={}]", wechatTicket);
//                jsTicket = wechatTicket.getTicket();
//                if (StringUtils.isEmpty(jsTicket)) {
//                    throw new IllegalArgumentException(wechatTicket.getErrmsg());
//                }
//            }
//            jsTicketCacheMap.put(corpId, jsTicket);
//        }
//        return jsTicket;
//    }
//
//    @Override
//    public String getWXJsapiTicket(WechatConfig wechatConfig,String type) {
//        String appId="";
//        if(WeChatTypeConstant.PERSIONAL_WECHAT.equals(type)){
//            appId=wechatConfig.getAppId();
//        }
//        if(WeChatTypeConstant.SERVICE.equals(type)){
//            appId=wechatConfig.getWxServiceAppId();
//        }
//        String jsTicket = jsTicketCacheMap.getIfPresent(appId);
//        if (StringUtils.isEmpty(jsTicket)) {
//            String accessToken = tokenCacheMap.getIfPresent(appId);
//            if (StringUtils.isEmpty(accessToken)) {
//                accessToken = getWxApiAccessToken(wechatConfig,type);
//            }
//            WechatTicket wechatTicket = restTemplate.getForObject(wxJsapiTicketUrl, WechatTicket.class, accessToken);
//            log.debug("getJsapiTicket.[wechatTicket={}]", wechatTicket);
//            jsTicket = wechatTicket.getTicket();
//            if (StringUtils.isEmpty(jsTicket)) {
//                // 处理极端场景 access_token 刚好失效
//                String accessTokenRefresh = getAccessToken(wechatConfig);
//                wechatTicket = restTemplate.getForObject(jsapiTicketUrl, WechatTicket.class, accessTokenRefresh);
//                log.debug("getJsapiTicket 2 .[wechatTicket={}]", wechatTicket);
//                jsTicket = wechatTicket.getTicket();
//                if (StringUtils.isEmpty(jsTicket)) {
//                    throw new IllegalArgumentException(wechatTicket.getErrmsg());
//                }
//            }
//            jsTicketCacheMap.put(appId, jsTicket);
//        }
//        return jsTicket;
//    }
//    @Override
//    public String getWXJsapiTicket(String tenantId,String type) {
//        WechatConfig wechatConfig = getWechatConfig(tenantId);
//        return getWXJsapiTicket(wechatConfig,type);
//
//    }
//
//
//    @Override
//    public WechatSignVO getQYWechatSignVO(String tenantId, String url) {
//        WechatConfig wechatConfig = getWechatConfig(tenantId);
//        Assert.notNull(wechatConfig, "无企业配置信息");
//        String ticket = getJsapiTicket(wechatConfig);
//        WechatSignVO wechatSignVO = new WechatSignVO(url);
//        String code = String.format(SIGN_FORMAT, ticket, wechatSignVO.getNonceStr(), wechatSignVO.getTimestamp(), url);
//        wechatSignVO.setSignature(sign(code));
//        wechatSignVO.setAppId(wechatConfig.getCorpId());
//        return wechatSignVO;
//    }
//
////    @Override
////    public WechatSignVO getWechatSignVO(String tenantId, String url,String type) {
////        WechatConfig wechatConfig = getWechatConfig(tenantId);
////        Assert.notNull(wechatConfig, "无企业配置信息");
////        String ticket = getWXJsapiTicket(wechatConfig,type);
////        WechatSignVO wechatSignVO = new WechatSignVO(url);
////        String code = String.format(SIGN_FORMAT, ticket, wechatSignVO.getNonceStr(), wechatSignVO.getTimestamp(), url);
////        wechatSignVO.setSignature(sign(code));
////        if(WeChatTypeConstant.SERVICE.equals(type)){
////            wechatSignVO.setAppId(wechatConfig.getWxServiceAppId());
////        }
////        if(WeChatTypeConstant.PERSIONAL_WECHAT.equals(type)){
////            wechatSignVO.setAppId(wechatConfig.getAppId());
////        }
////        return wechatSignVO;
////    }
//
////    @Override
////    public WechatScheme getWechatScheme(String tenantId, WechatSchemeReq wechatSchemeReq) {
////        WechatConfig wechatConfig = getWechatConfig(tenantId);
////        Assert.notNull(wechatConfig, "无企业配置信息");
////        String appId = wechatConfig.getAppId();
////        String accessToken = tokenCacheMap.getIfPresent(appId);
////        if (StringUtils.isEmpty(accessToken)) {
////            accessToken = getApiAccessToken(wechatConfig);
////        }
////        WechatScheme wechatScheme = restTemplate.postForObject(schemeUrl, wechatSchemeReq, WechatScheme.class, accessToken);
////        log.debug("getWechatScheme.[wechatScheme={}]", wechatScheme);
////        if (StringUtils.isEmpty(wechatScheme.getOpenlink())) {
////            accessToken = getApiAccessToken(wechatConfig);
////            wechatScheme = restTemplate.postForObject(schemeUrl, wechatSchemeReq, WechatScheme.class, accessToken);
////            log.debug("getWechatScheme 2 .[wechatScheme={}]", wechatScheme);
////            if (StringUtils.isEmpty(wechatScheme.getOpenlink())) {
////                throw new IllegalArgumentException(wechatScheme.getErrmsg());
////            }
////        }
////        return wechatScheme;
////    }
//
////    public String getApiAccessToken(WechatConfig wechatConfig) {
////        WechatToken wechatToken = restTemplate.getForObject(wxTokenUrl, WechatToken.class, wechatConfig.getAppId(), wechatConfig.getAppSecret());
////        log.debug("getApiAccessToken.[wechatToken={}]", wechatToken);
////        String accessToken = wechatToken.getAccess_token();
////        if (StringUtils.isEmpty(accessToken)) {
////            throw new IllegalArgumentException(wechatToken.getErrmsg());
////        }
////        tokenCacheMap.put(wechatConfig.getAppId(), accessToken);
////        return accessToken;
////    }
////    public String getWxApiAccessToken(WechatConfig wechatConfig,String type) {
////        String secrectId = "";
////        String appId="";
////        if(WeChatTypeConstant.PERSIONAL_WECHAT.equals(type)){
////            secrectId=wechatConfig.getAppSecret();
////            appId=wechatConfig.getAppId();
////        }
////        if(WeChatTypeConstant.SERVICE.equals(type)){
////            secrectId=wechatConfig.getWxServiceAppSecrect();
////            appId=wechatConfig.getWxServiceAppId();
////        }
////        WechatToken wechatToken = restTemplate.getForObject(wxTokenUrl, WechatToken.class, appId, secrectId);
////        log.debug("getApiAccessToken.[wechatToken={}]", wechatToken);
////        String accessToken = wechatToken.getAccess_token();
////        if (StringUtils.isEmpty(accessToken)) {
////            throw new IllegalArgumentException(wechatToken.getErrmsg());
////        }
////        tokenCacheMap.put(wechatConfig.getAppId(), accessToken);
////        return accessToken;
////    }
//
//    /**
//     * 验签
//     *
//     * @param code
//     * @return
//     */
//    String sign(String code) {
//        try {
//            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
//            crypt.reset();
//            crypt.update(code.getBytes("UTF-8"));
//            Formatter formatter = new Formatter();
//            final byte[] hash = crypt.digest();
//            for (byte b : hash) {
//                formatter.format("%02x", b);
//            }
//            String sign = formatter.toString();
//            formatter.close();
//            return sign;
//        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//}