package cn.zhangyou705.wechatlove.util;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author ZhangYou
 * @description
 * @date 2022/8/23
 */
@Component
public class WeatherUtil {

    @Value("${weather.ak}")
    private String ak;

    @Value("${weather.areaCode}")
    private String areaCode;

    public JSONObject getTodayWeatherByBaiDu() {
        String result;
        JSONObject todayWeather = new JSONObject();
        try {
            result = HttpUtil.get("https://api.map.baidu.com/weather/v1/?district_id=" + areaCode + "&data_type=all&ak=" + ak);
            JSONObject jsonObject = JSONObject.parseObject(result);
            if ("success".equals(jsonObject.getString("message"))) {
                JSONArray arr = jsonObject.getJSONObject("result").getJSONArray("forecasts");
                todayWeather = arr.getJSONObject(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return todayWeather;
    }


    public JSONObject getTodayWeatherByLocation(String longitude, String latitude) {

        String adcode = getAddressInfoByLngAndLat(longitude, latitude);

        if (!StringUtils.isEmpty(adcode)) {
            areaCode = adcode;
        }

        String result;
        JSONObject todayWeather = new JSONObject();
        try {
            result = HttpUtil.get("https://api.map.baidu.com/weather/v1/?district_id=" + areaCode + "&data_type=all&ak=" + ak);
            JSONObject jsonObject = JSONObject.parseObject(result);
            if ("success".equals(jsonObject.getString("message"))) {
                JSONArray arr = jsonObject.getJSONObject("result").getJSONArray("forecasts");
                todayWeather = arr.getJSONObject(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(todayWeather);
        return todayWeather;
    }


    /**
     * @author Katie Qu
     * @date 2021/4/25
     */
//    public class BaiduMapGeocoderUtil {
//
//        /**
//         * 百度地图 Api调用相关的百度AK  服务端ak(密钥)
//         */
//        public final static String BAIDU_MAP_AK = "xxxxx";
//        //测试使用
//        public static void main(String[] args) {
//            getTodayWeatherByLocation("113.82","31.62");
//        }

    /**
     * 根据经纬度调用百度API获取 地理位置信息，根据经纬度
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @return
     */
    public String getAddressInfoByLngAndLat(String longitude, String latitude) {
        JSONObject obj = new JSONObject();
        String location = latitude + "," + longitude;
        //百度url  coordtype :bd09ll（百度经纬度坐标）、bd09mc（百度米制坐标）、gcj02ll（国测局经纬度坐标，仅限中国）、wgs84ll（ GPS经纬度）
        String url = "http://api.map.baidu.com/reverse_geocoding/v3/?ak=" + ak + "&output=json&coordtype=wgs84ll&location=" + location;
        try {
            String json = loadJSON(url);
            obj = JSONObject.parseObject(json);
//                System.out.println(obj.toString());
            // status:0 成功
            String success = "0";
            String status = String.valueOf(obj.get("status"));
            if (success.equals(status)) {
                String result = String.valueOf(obj.get("result"));
                JSONObject resultObj = JSONObject.parseObject(result);
                JSONObject addressComponent = resultObj.getJSONObject("addressComponent");
                //JSON字符串转换成Java对象
//                     AddressComponent addressComponentInfo = JSONObject.parseObject(addressComponent, AddressComponent.class);
//                    System.out.println("addressComponentInfo:"+addressComponent);
                String adcode = addressComponent.getString("adcode");

                return adcode;
            }
        } catch (Exception e) {
            System.out.println("未找到相匹配的经纬度，请检查地址！");
        }
        return null;
    }

    public String loadJSON(String url) {
        StringBuilder json = new StringBuilder();
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return json.toString();
    }
}
